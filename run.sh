#!/usr/bin/env sh
set -m

API_URL="http://localhost:8086"
CONFIG_FILE="/etc/influxdb/influxdb.conf"
ADMIN_USER="root"
ADMIN_PASS="secopstech"


# Start influx with $CONF_FILE
echo "=> Influxd starting..."
exec influxd -config ${CONFIG_FILE} &

#Wait for the startup of influxdb
RET=1
while [[ $RET -ne 0 ]]; do
    echo "=> Waiting for confirmation of InfluxDB service startup..."
    sleep 3
    wget -O /dev/null ${API_URL}/ping > /dev/null 2>&1
    RET=$?
done
echo ""

if [[ ! -f /.influx-configured ]]; then
  # Create an admin user
echo "=> Creating admin user..."
influx -host=localhost -port=8086 -execute="CREATE USER ${ADMIN_USER} WITH PASSWORD '${ADMIN_PASS}' WITH ALL PRIVILEGES"

touch /.influx-configured

fi

# Run influxd on foreground
fg