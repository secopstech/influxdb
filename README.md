# Overview
An alpine based influxdb that creates an admin user and sets auth enable.

## Default credentials
User: root
Pass: secopstech

## Run

```
docker run --name influxdb -it -d -p 8086:8086 -p 8083:8083 secopstech/influxdb:latest
```

## Admin UI Access

http://x.x.x.x:8083

